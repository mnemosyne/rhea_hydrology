"""
Runoff generation algorithms
"""

##======================================================================================================================##
##                ##
##======================================================================================================================##

import argparse
import doctest

import datetime


import numpy

##======================================================================================================================##
##                CLASS                                                                                                 ##
##======================================================================================================================##



class Runoff_Excess:
    """ Class doc """
    
    def __init__(self, typ="GA", ):
        """ Class initialiser """
        pass






class Runoff_Excess_Green_Ampt:
    """ 
    Green & Ampt excess model

    """
    
    def __init__(self, ks, hf, theta_f):
        """ Class initialiser 
        ks : 
        hf :
        
        """
        assert ks > 0
        #~ assert hf < 0
        self.theta_f = theta_f
        self.ks = ks # hydraulic conductivity
        self.hf = hf # suction head at the wetting front
            
    def runoff_evt(self, rain_amounts, theta_i, t_step):
        """
        Function doc
        PARAM   :    DESCRIPTION
        RETURN  :    DESCRIPTION
        
        >>> 'afarie'
        """
        assert (rain_amounts >= 0).all()
        assert rain_amounts[0] > 0
        assert self.hf >= 0
        
        dt = t_step.total_seconds()
        d_theta = self.theta_f - theta_i
        
        assert 0 < d_theta < 1, "pb theta"
        
        zf = 0
        inf_cap = numpy.inf
        
        zfs = []
        infcaps = []
        runoffs = []
        runinfs = []
        
        for i_rr in rain_amounts:
            r_inf = min([inf_cap, i_rr])# infiltrated water = infiltration capacity or rainfall rates
            runoff = i_rr - r_inf        # runoff : may be negative, remove < 0 latter
            #in list
            runinfs.append(r_inf)
            runoffs.append(runoff)
            zfs.append(zf)
            infcaps.append(inf_cap)
            #for the next time step
            d_zf = r_inf / d_theta     # advance of wetting front
            zf += d_zf                # new wetting front : z increase but hyd. head decrease
            print(d_zf, zf)
            h_zf = zf + self.hf        # hydraulic head at wetting front (zf+hf): both pos. here but neg. in reality
            h_gd = h_zf / zf           # hydraulic gradient between surface and wetting front
            print(h_gd)
            inf_int = self.ks * h_gd      # compute new infiltration capacity rate (m.s-1) using darcy law
            inf_cap = inf_int * dt        # compute new infiltration capacity amount (m)

        runoffs = numpy.array(runoffs, dtype=float)
        runinfs = numpy.array(runinfs, dtype=float)
        infcaps = numpy.array(infcaps, dtype=float)
        zfs = numpy.array(zfs, dtype=float)

        runoffs[runoffs < 0] = 0
        assert runoffs.min() >= 0
        assert runoffs.sum() + runinfs.sum() == rain_amounts.sum()
        print('check runoff')
        print('check len')

        res = {'runoff' : runoffs, "inf_capacity" : infcaps, "zf" : zfs, "runinf" : runinfs}
        
        return res


    def plot_evt(self):
        """
        Function doc
    
        PARAM   :    DESCRIPTION
        RETURN  :    DESCRIPTION
        
        >>> 'afarie'
        """
        

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                   #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)

    else:
        
        rrs = numpy.random.exponential(scale=10, size=100) * 1e-4
        ks0 = 10e-6 
        hf0 = 1
        thf0 = 0.5
        td = datetime.timedelta(seconds=15)

        rga = Runoff_Excess_Green_Ampt(ks=ks0, hf=hf0, theta_f=thf0)
