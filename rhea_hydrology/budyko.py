"""

"""



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os
import argparse
import doctest
import numpy

from matplotlib import pyplot


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def evapotranspiration_ratio(p, e):
    """
    Function doc
    >>> 'afarie'
    """
    res = e/p
    return res
    
def aridity_index(p, pet):
    """
    Function doc
    >>> 'afarie'
    """
    res = pet/p
    return res
    
def fu_equation(arid_index, w):
    """
    Function doc
    >>> 'afarie'
    """
    res = 1 + arid_index - (1 + arid_index**w)**(1/w)
    return res
    


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    ARGPARSE                   #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)



    #+++++++++++++++++++++++++++++++#
    #    TEST                       #
    #+++++++++++++++++++++++++++++++#
    
    precips = numpy.linspace(400,2000)
    pot_evaps= numpy.linspace(400,2000)
    arid_indexes = numpy.linspace(0.5,5)
    
    fig = pyplot.figure()
    ax = fig.gca()
    
    for i in numpy.arange(1,10,0.5):
        budyks = fu_equation(arid_indexes, w=i)
        pyplot.plot(arid_indexes, budyks, label="w={0:.1f}".format(i))
        
    ax.set(xlabel="Aridity index (ETP/P)", ylabel="Evaporation ratio (E/P)")
    
    pyplot.legend()
    pyplot.grid(1)
    
    fig.show()
    
